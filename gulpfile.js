var elixir = require('laravel-elixir');

require('laravel-elixir-bowerbundle');
require('laravel-elixir-vueify');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    // first backend
    mix.bower(['jquery', 'font-awesome', 'bootstrap-sass', 'bootstrap-sweetalert', 'select2', 'pusher', 'growl','datatables', 'bootstrap-datepicker','jQuery.print'])
        .sass('app.scss', 'public/css/app.css')
        .browserify('main.js')

        .styles(['css/app.css'], 'public/css/all.css', 'public')

        // copy fonts and images
        .copy('vendor/bower_components/font-awesome/fonts', 'public/build/css')
        .copy('vendor/bower_components/bootstrap-sass/assets/fonts/bootstrap', 'public/build/fonts/bootstrap')
        .copy('vendor/bower_components/datatables/media/images', 'public/build/css')

        .version(['css/all.css', 'js/all.js', 'css/font-awesome*']);
});
