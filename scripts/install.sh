#!/usr/bin/env bash

source "$(dirname $0)/guard.sh"


SHOWHELP=1

if [[ "$1"=="development" ]] || [[ "$1"=="production" ]]; then
    SHOWHELP=0
fi

if [[ "$@" == *"--help"* ]] || [[ SHOWHELP == 1 ]]; then

    if [[ SHOWHELP == 1 ]] && [[ "$@" != *"--help"* ]]; then
         echo "Error: no mode of operation specified";
    fi

    echo "Usage: install.sh <operation> [<args>]                                          "
    echo "  Modes of operation:                                                           "
    echo "    development:  run all scripts/command in dev modus                          "
    echo "    production:   install application for production usage                      "
    echo "  Arguments:                                                                    "
    echo "    --help       show this message											  "
    echo "    --hooks      install git post-merge hook									  "
    echo "    --seed       run seeder for development or production (depends on operation)"
    exit
fi


if [[ "$@" = "--hooks" ]]; then
    echo "[--- running ./scripts/install_hooks.sh ---]"
    ./scripts/install_hooks.sh
fi


echo "[--- Downloading composer packages ---]"
composer install


echo "[--- Running php artisan key:generate ---]"
php artisan key:generate


echo "[--- Running php artisan migrate ---]"
php artisan migrate


if [[ "$1" == "development" ]] ;then
        echo "[--- Running php artisan migrate --database=mysql_test ---]"
        php artisan migrate --database=mysql_test

        if [[ "$@" == *"--seed"* ]]; then
            echo "[--- running php artisan db:seed --class=DevelopmentSeeder ---]"
            php artisan db:seed --class=DevelopmentSeeder
        fi

        echo "[--- Running php artisan ide-helper:generate ---]"
        php artisan ide-helper:generate
	else
        if [[ "$@" == *"--seed"* ]]; then
            echo "[--- running php artisan db:seed ---]"
            php artisan db:seed
		fi
fi

echo "[--- Running ./scripts/npm.sh ---]";
sh ./scripts/npm.sh

echo "[--- Running ./scripts/bower.sh ---]";
sh ./scripts/bower.sh

echo "[--- Running ./scripts/gulp.sh ---]";
sh ./scripts/gulp.sh


if [[ "$@" == *"--hooks"* ]]; then
    echo "[--- running ./scripts/install_hooks.sh ---]"
    sh ./scripts/install_hooks.sh
fi


echo "[--- Running php artisan search:rebuild ---]"
php artisan search:rebuild


echo "Done."
