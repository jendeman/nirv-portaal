#!/bin/sh

exit 0
cd /home/forge/parkitectnexus.com
php artisan down
git pull origin master
composer install --no-interaction --prefer-dist
php artisan migrate --force
php artisan route:cache
php artisan laroute:generate
php artisan up
npm install
bower install
gulp
