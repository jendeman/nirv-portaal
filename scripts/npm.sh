#!/usr/bin/env bash

source "$(dirname $0)/guard.sh"

echo "[--- Installing npm packages ---]"

npm install
