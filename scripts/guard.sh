#!/usr/bin/env bash


DIR=$(basename `pwd`)

if [ $DIR == "scripts" ]
        then
		cd ..
fi

if [ ! -f "composer.json" ]
	then
		echo "ERROR: Run this script from the project root."
		exit
fi
